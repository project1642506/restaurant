import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrl: './items.component.css'
})
export class ItemsComponent implements OnInit{

  showFooter: boolean = false;
  
  constructor(private cartService: CartService){

  }
  
  ngOnInit(){
    
  }

  addToCart(item: any) {
    this.cartService.addToCart(item);
  }

}

// cart.service.ts
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  cartItems: any;
  private cartItemsSubject = new BehaviorSubject<any[]>([]);
  cartItems$ = this.cartItemsSubject.asObservable();
  totalAmount: number = 0;

  constructor(private http: HttpClient) {
    this.cartItems = [];
  }

  addToCart(item: any) {
    const existingItemIndex = this.cartItems.findIndex((cartItem: any) => cartItem.name === item.name);

    if (existingItemIndex !== -1) {
      this.cartItems[existingItemIndex].quantity += 1;
    } else {
      this.cartItems.push({ ...item, quantity: 1 });
    }

    this.cartItemsSubject.next([...this.cartItems]);
    this.updateTotalAmount();
  }

  private updateTotalAmount() {
    const totalAmount = this.cartItems.reduce((sum: number, item: any) => {
      const itemPrice = item.price || 0;
      const itemQuantity = item.quantity || 1;
      return sum + (itemPrice * itemQuantity);
    }, 0);

    this.totalAmount = totalAmount;
    console.log('Updated total amount:', totalAmount);
  }

  // Other methods for managing the cart items...
}

import { CanActivateFn } from '@angular/router';
import { RestaurantService } from './restaurant.service';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state) => {
  let service = inject(RestaurantService);
  return service.getIsUserLogged();
};

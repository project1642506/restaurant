// for google authentication service
declare var google:any;
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestaurantService } from '../restaurant.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {

  showFooter: boolean = false;

  captcha: string = '';
  captchaInput: string = '';
  protected aFormGroup: FormGroup;
  isCaptchaValid: boolean = false;
  captchaResolved: boolean = false;
  siteKey: string = "6LeJjWgpAAAAALvszYULjRzlg7eUUo_49Q_wsHnO";
  showRegistrationForm: boolean = false;
  Restaurant: any;

  constructor(private router: Router, private service: RestaurantService,private toastr: ToastrService, private formBuilder: FormBuilder) {
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    });
   }

   onCaptchaResolved(event: any) {
    // Handle captcha resolved event
    this.isCaptchaValid = true;
    this.captchaResolved = true;
  }
  
  ngOnInit(): void {
    google.accounts.id.initialize({
      client_id: '1044766496748-pp7j7lbq9kahsjut6auojabv4ai1e9j8.apps.googleusercontent.com',
      callback: (resp: any)=>this.handleLogin(resp)
    });

    google.accounts.id.renderButton(document.getElementById("google-btn"),{
      theme: 'filled_blue',
      size: 'medium',
      shape: 'rectangle',
      width: 350
    })
  }

  private decodeToken(token:string){
    return JSON.parse(atob(token.split(".")[1]));
  }

  handleLogin(response: any){
    if(response){
      const payLoad = this.decodeToken(response.credential);
      sessionStorage.setItem("loggedInUser", JSON.stringify(payLoad));
      this.router.navigate(['items']);
      this.service.setIsUserLoggedIn();  
    }
  }

  async loginSubmit(loginForm: any) {
    if (loginForm.email === 'Admin' && loginForm.password === 'Admin') {
      this.service.setIsUserLoggedIn();
      localStorage.setItem("email", loginForm.email);
      this.router.navigate(['customers']);
    } else {
      try {
        const data = await this.service.customerLogin(loginForm.email, loginForm.password);
        if (data != null) {
          this.service.setIsUserLoggedIn();
          localStorage.setItem("email", loginForm.email);
          this.router.navigate(['items']);
        } else {
          this.toastr.error('Invalid Credentials', 'Error', {
            closeButton: true,
            progressBar: true,
            positionClass: 'toast-top-right',
            tapToDismiss: false,
            timeOut: 2000, // 2 seconds
          });
        }
      } catch (error) {
        console.error("Error logging in:", error);
        this.toastr.error('An error occurred while logging in', 'Error', {
          closeButton: true,
          progressBar: true,
          positionClass: 'toast-top-right',
          tapToDismiss: false,
          timeOut: 2000, // 2 seconds
        });
      }
    }
  }
  

  register(formData: any) {
   
  }

}
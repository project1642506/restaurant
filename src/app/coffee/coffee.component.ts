import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-coffee',
  templateUrl: './coffee.component.html',
  styleUrl: './coffee.component.css'
})
export class CoffeeComponent {

  coffees = [
    { name: 'Espresso', image: 'assets/espresso.jpeg', description: 'Intense and concentrated coffee shot, perfect for the bold coffee lover.', price: 99.99 },
    { name: 'Cappuccino', image: 'assets/cappuccino.jpeg', description: 'Rich espresso combined with steamed milk and topped with frothy foam.', price: 100.00 },
    { name: 'Cafe Mocha', image: 'assets/mocha.png', description: 'Espresso, hot chocolate, and steamed milk topped with whipped cream.', price: 150.00 },
    { name: 'Americano', image: 'assets/americano.webp', description: 'Diluted espresso with hot water, providing a strong and smooth flavor.', price: 80.00 },
    { name: 'Frappuccino', image: 'assets/frappucino.webp', description: 'Blended coffee beverage with ice and various flavors, perfect for a refreshing treat.', price: 69.99 },
    { name: 'Iced Latte', image: 'assets/icedlattee.jpeg', description: 'Espresso mixed with chilled milk, served over ice for a delightful cool drink.', price: 100.00 },
    { name: 'Caramel Macchiato', image: 'assets/caramelmacchiato.jpg', description: 'Espresso with steamed milk and vanilla syrup, marked with caramel drizzle.', price: 50.00 },
    { name: 'Cold Brew', image: 'assets/coldbrew.jpg', description: 'Smooth and cold steeped coffee, offering a bold and less acidic flavor.', price: 60.00 },
    { name: 'Black Coffee', image: 'assets/blackcoffee.jpg', description: 'Classic black coffee brewed to perfection, simple and strong.', price: 50.00 },
];

showSuccessMessage: boolean = false;
constructor(private router: Router, private cartService: CartService) { }

addToCart(coffee: any) {
  this.cartService.addToCart(coffee);
 
  this.showSuccessMessage = true;
  setTimeout(() => {
    this.showSuccessMessage = false;
  }, 3000);
}

}

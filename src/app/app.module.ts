import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ItemsComponent } from './items/items.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { CartComponent } from './cart/cart.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { RecaptchaModule } from 'ng-recaptcha';
import { RegisterComponent } from './register/register.component';
import { ToastrModule } from 'ngx-toastr';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { LogoutComponent } from './logout/logout.component';
import{BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { CustomerComponent } from './customer/customer.component';
import { GenderPipe } from './gender.pipe';
import { CoffeeComponent } from './coffee/coffee.component';
import { DessertsComponent } from './desserts/desserts.component';
import { FoodComponent } from './food/food.component';
import { SnacksComponent } from './snacks/snacks.component';
import { MocktailsComponent } from './mocktails/mocktails.component';
import { TableComponent } from './table/table.component';
import { OrderNowComponent } from './order-now/order-now.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ItemsComponent,
    AboutusComponent,
    CartComponent,
    LoginComponent,
    RegisterComponent,
    AdminComponent,
    LogoutComponent,
    HomeComponent,
    CustomerComponent,
    GenderPipe,
    CoffeeComponent,
    DessertsComponent,
    FoodComponent,
    SnacksComponent,
    MocktailsComponent,
    TableComponent,
    OrderNowComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    BrowserModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RecaptchaModule,
    NgxCaptchaModule,
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

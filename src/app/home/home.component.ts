import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  imageUrls: string[] = [
    "/../assets/Photos/Screenshot 2024-02-20 153408.png",
    "/../assets/Photos/WhatsApp Image 2024-02-01 at 2.57.17 PM (1).jpeg",
    "/../assets/Photos/WhatsApp Image 2024-02-01 at 2.57.17 PM (1).jpeg"
  ];
}

import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../restaurant.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
 
  customers: any;
  emailId: any;
  countries: any;

  constructor(private service: RestaurantService) {
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.service.getCustomers().subscribe((data: any) => {
      this.customers = data;
      this.service.getAllCountries().subscribe((data: any) => { 
        this.countries = data; 
      });
    });
  }

  editCustomer(cust: any) {
    cust.isEditing = true;
  }

  updateCustomer(cust: any) {
    this.service.updateCustomer(cust).subscribe((data: any) => {
      // Handle response or any other logic
      cust.isEditing = false;
    });
  }
  
  cancelEdit(cust: any) {
    cust.isEditing = false;
  }

  deleteCustomer(cust: any) {
    this.service.deleteCustomer(cust.custId).subscribe((data: any) => {
      console.log(data);
      // Handle response or any other logic
    });

    const i = this.customers.findIndex((element: any) => {
      return element.custId == cust.custId;
    });

    this.customers.splice(i, 1);

    alert('Customer Deleted Successfully!!!');
  }
}

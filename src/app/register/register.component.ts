import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../restaurant.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  showFooter: boolean = false;
  
  countries: any;
  restaurant: any = {}; // Initialize restaurant object to avoid null issues
  confirmPassword: string = '';
  phNumber: string = '';

  constructor(private service: RestaurantService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
  }

  registerSubmit(regForm: any) {
    if (regForm.password !== this.confirmPassword) {
      this.toastr.error('Password and Confirm Password must be the same.');
      return;
    }

    this.restaurant.country = regForm.country;
    this.restaurant.custname = regForm.custName;
    this.restaurant.email = regForm.emailId;
    this.restaurant.gender = regForm.gender;
    this.restaurant.password = regForm.password;
    this.restaurant.phoneNumber = regForm.phNumber;
    this.restaurant.otp = ''; // Initialize otp field

    console.log(this.restaurant);

    this.service.registerCustomer(this.restaurant).subscribe((data: any) => {
        console.log(data);
        this.toastr.success('Registration Successful');
        this.router.navigate(['login']);
    }, (error: any) => {
        console.error('Error registering customer:', error);
        this.toastr.error('Registration Failed');
    });
  }
}

import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent {
  name!: string;
  email!: string;
  phone!: string;
  date!: string;
  time!: string;
  guests!: number;

  constructor(private http: HttpClient) { }

  submitBookingForm() {
    const tableBooking = {
      name: this.name,
      email: this.email,
      phone: this.phone,
      date: this.date,
      time: this.time,
      guests: this.guests
    };

    this.http.post<any>('http://localhost:8085/bookTable', tableBooking)
      .subscribe(response => {
        console.log('Table booking successful:', response);
        // Reset form fields
        this.name = '';
        this.email = '';
        this.phone = '';
        this.date = '';
        this.time = '';
        this.guests = 0;
      }, error => {
        console.error('Error booking table:', error);
      });
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { authGuard } from './auth.guard';
import { AboutusComponent } from './aboutus/aboutus.component';
import { CartComponent } from './cart/cart.component';
import { LogoutComponent } from './logout/logout.component';
import { ItemsComponent } from './items/items.component';
import { HomeComponent } from './home/home.component';
import { CustomerComponent } from './customer/customer.component';
import { CoffeeComponent } from './coffee/coffee.component';
import { DessertsComponent } from './desserts/desserts.component';
import { FoodComponent } from './food/food.component';
import { SnacksComponent } from './snacks/snacks.component';
import { MocktailsComponent } from './mocktails/mocktails.component';
import { TableComponent } from './table/table.component';
import { OrderNowComponent } from './order-now/order-now.component';

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'home', component:HomeComponent},
  {path:'login', component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'table', canActivate:[authGuard],component:TableComponent},
  {path:'order',component:OrderNowComponent},
  {path:'cart',canActivate:[authGuard],component:CartComponent},
  {path:'logout', canActivate:[authGuard],component:LogoutComponent},
  {path:'items', component:ItemsComponent},
  {path:'aboutus', canActivate:[authGuard],component:AboutusComponent},
  {path:'customers', canActivate:[authGuard],component:CustomerComponent},
  {path:'coffee', canActivate:[authGuard],component:CoffeeComponent},
  {path:'desserts', canActivate:[authGuard],component:DessertsComponent},
  {path:'food', canActivate:[authGuard],component:FoodComponent},
  {path:'snacks', canActivate:[authGuard],component:SnacksComponent},
  {path:'mocktails', canActivate:[authGuard],component:MocktailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';

declare var Razorpay: any;

@Component({
  selector: 'app-order-now',
  templateUrl: './order-now.component.html',
  styleUrls: ['./order-now.component.css'],
})
export class OrderNowComponent implements OnInit {

  custEmail: any;
  totalAmount: any;
  cartItems: any[] = [];
  showPaymentDetails: boolean = false;
  paymentDetails: any = {
    name: '',
    email: '',
    phone: ''
  };

  constructor(private http: HttpClient, private router: Router, private cartService: CartService) { }

  ngOnInit(): void {
    this.custEmail = localStorage.getItem('emailId');
    this.cartItems = this.cartService.cartItems;
    this.calculateTotal();
    this.loadRazorpay(); // Ensure Razorpay is loaded when the component initializes
  }

  calculateTotal(): void {
    this.totalAmount = this.cartItems.reduce((sum, item) => {
      const itemPrice = item.price || 0;
      const itemQuantity = item.quantity || 1;
      return sum + (itemPrice * itemQuantity);
    }, 0);
  }

  loadRazorpay(): void {
    const script = document.createElement('script');
    script.src = 'https://checkout.razorpay.com/v1/checkout.js';
    script.async = true;
    script.onload = () => {
      console.log('Razorpay script loaded');
    };
    document.head.appendChild(script);
  }

  openPaymentForm(): void {
    this.showPaymentDetails = true;
  }

  PayNow(): void {
    const RozorpayOptions = {
      description: 'Sample Razorpay demo',
      currency: 'INR',
      amount: this.totalAmount * 100, // Amount should be in paisa/cents
      name: 'Vedavyasa',
      key: 'rzp_test_IIIcjdY3T2zbMj',
      prefill: {
        name: this.paymentDetails.name,
        email: this.paymentDetails.email,
        contact: this.paymentDetails.phone
      },
      theme: {
        color: '#6466e3'
      },
      modal: {
        ondismiss: () => {
          console.log('Payment dismissed');
        }
      }
    };

    const successCalback = (paymentid: any) => {
      console.log(paymentid);
    };

    const failureCallback = (error: any) => {
      console.error(error);
    };

    if (typeof Razorpay !== 'undefined') {
      Razorpay.open(RozorpayOptions, successCalback, failureCallback);
    } else {
      console.error('Razorpay has not been loaded yet.');
    }
  }
}

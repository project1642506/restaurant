import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrl: './food.component.css'
})
export class FoodComponent {
  foods = [
    { name: 'Crispy Corn', image: 'assets/crispycorn.jpeg', description: 'Delicious crispy corn with a perfect blend of spices.', price: 150.00 },
    { name: 'Kebabs', image: 'assets/kebabs.jpg', description: 'Mouth-watering kebabs grilled to perfection.', price: 200.00 },
    { name: 'Noodles', image: 'assets/noodles.jpeg', description: 'Tasty noodles cooked with fresh vegetables and sauces.', price: 100.00 },
    { name: 'Pasta', image: 'assets/pastas.jpg', description: 'Creamy pasta with a rich tomato sauce.', price: 140.00 },
    { name: 'Butter Chicken', image: 'assets/butterchicken.jpg', description: 'Succulent butter chicken curry served with naan.', price: 250.00 },
    { name: 'Paneer', image: 'assets/paneer.jpg', description: 'Paneer tikka masala with a flavorful gravy.', price: 180.00 },
    { name: 'Biryani', image: 'assets/biryani.jpg', description: 'Fragrant biryani with aromatic spices and tender meat.', price: 250.00 },
    { name: 'Pulao', image: 'assets/pulao.jpg', description: 'Vegetable pulao with basmati rice and assorted veggies.', price: 120.00 },
    { name: 'Fried Rice', image: 'assets/friedrice.jpg', description: 'Classic fried rice with a mix of vegetables and soy sauce.', price: 150.00 }
  ];
  

  showSuccessMessage: boolean = false;
constructor(private router: Router, private cartService: CartService) { }

addToCart(coffee: any) {
  this.cartService.addToCart(coffee);
 
  this.showSuccessMessage = true;
  setTimeout(() => {
    this.showSuccessMessage = false;
  }, 3000);
}

}




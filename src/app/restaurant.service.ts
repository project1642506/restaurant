import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  getcustomer() {
    throw new Error('Method not implemented.');
  }
  
  isUserLoggedIn: boolean;
  loginStatus: any;
  cartItems: any;

  constructor(private http: HttpClient) { 
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
    this.cartItems = [];
  }

  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }  

  getloginStatus(): any {
    return this.loginStatus.asObservable();
  }

  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }

  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  customerLogin(email: any, password: any): any {
    return this.http.get('http://localhost:8085/customerLogin/' + email + '/' + password).toPromise();
  }

  registerCustomer(register: any): any {
    return this.http.post('http://localhost:8085/addCustomer', register);
  }

  getCustomers():any{
    return this.http.get('http://localhost:8085/getCustomers')
  }

  updateCustomer(customer: any) {
    return this.http.put('http://localhost:8085/updateCustomer', customer);
  }

  deleteCustomer(custId: any) {
    return this.http.delete('http://localhost:8085/deleteCustomerById/' + custId);
  }

  // Method to set item in local storage
  setItem(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

   //Cart using Service
   addToCart(snacks: any) {
    this.cartItems.push(snacks);
  }
  getCartItems(): any {
    return this.cartItems;
  }

  removeFromCart(itemId:any){
    this.cartItems = this.cartItems.filter((item:any) =>
      item.id !== itemId);
  }

  addItem(item: any){

    return this.http.post('http://localhost:8085/addItem', item);
  }
}

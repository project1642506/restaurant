import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestaurantService } from '../restaurant.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent implements OnInit{



  constructor(private router:Router, private service:RestaurantService){
    localStorage.removeItem('emailId');
    localStorage.clear();

    this.service.setIsUserLoggedOut();

    this.router.navigate(['login']);
  }
  ngOnInit(){
    
  }


}

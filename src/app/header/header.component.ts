import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../restaurant.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit{

  showFooter: boolean = true;

  loginStatus:any;
  cartItems: any;

  constructor(private service :RestaurantService){

    this.cartItems = service.getCartItems();
    
  }

  ngOnInit(){
    this.service.getloginStatus().subscribe((data:any)=>{
      this.loginStatus=data;
    });
  }

}
